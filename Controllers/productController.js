const Product = require("../Models/productsSchema.js");
const auth = require("../auth.js");
const mongoose = require("mongoose");


/*
	PRODUCT MANIPULATIONS
*/

module.exports.addProduct = (request, response) =>{
	let input = request.body;
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin) {
		let newItem = new Product({
			name: input.name,
			category: input.category,
			description: input.description,
			price: input.price,
			stocks: input.stocks,
			image: input.image
		});

		return newItem.save()
		.then(product => response.send(product))
		.catch(error => response.send(false))
	}
	else {
		return response.send(false);
	}
}

module.exports.updateProduct = (request, response) => {
	let product = request.body;
	const id = request.params.id
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin) {
		Product.findOneAndUpdate({_id: id},{
			name : product.name,
			category : product.category,
			description: product.description,
			price: product.price,
			stocks : product.stocks,
			itemDiscount : product.discount
		},{new:true})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}
	else {
		return response.send(false);
		}
}

module.exports.archiveProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const id = request.params.id;

	if(userData.isAdmin) {
		Product.findByIdAndUpdate({_id: id},{stocks: 0},{new:true})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}
	else {
		return response.send(false);
	}
}

/*
	VIEW PRODUCTS
*/

module.exports.viewProducts = (request, response) => {
	Product.find({stocks: {$ne: 0}})
	.then(result => response.send(result))
	.catch(error => response.send(false))
}

module.exports.viewMens = (request, response) => {
	Product.find({category: "mens",stocks: {$ne: 0}})
	.then(result => response.send(result))
	.catch(error => response.send(false))
}

module.exports.viewWomens = (request, response) => {
	Product.find({category: "womens",stocks: {$ne: 0}})
	.then(result => response.send(result))
	.catch(error => response.send(false))
}

module.exports.viewToddlers = (request, response) => {
	Product.find({category: "toddlers",stocks: {$ne: 0}})
	.then(result => response.send(result))
	.catch(error => response.send(false))
}

module.exports.viewAppliances = (request, response) => {
	Product.find({category: "appliances",stocks: {$ne: 0}})
	.then(result => response.send(result))
	.catch(error => response.send(false))
}

module.exports.viewTools = (request, response) => {
	Product.find({category: "tools",stocks: {$ne: 0}})
	.then(result => response.send(result))
	.catch(error => response.send(false))
}

module.exports.viewOthers = (request, response) => {
	Product.find({category: "others",stocks: {$ne: 0}})
	.then(result => response.send(result))
	.catch(error => response.send(false))
}

module.exports.archivedProducts = (request, response) => {
	Product.find({stocks: 0})
	.then(result => response.send(result))
	.catch(error => response.send(false))
}

module.exports.viewItem = (request, response) => {
	const _id = request.params.id;

	Product.findById({_id})
	.then(result => response.send(result))
	.catch(error => response.send(false))
}

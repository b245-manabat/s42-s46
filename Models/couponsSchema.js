const mongoose = require("mongoose");

const couponsSchema = new mongoose.Schema({
	couponCode: {
		type : String,
		required : [true, "Enter a unique code for your coupon."]
	},
	couponDiscount : {
		type : Number,
		required : [true, "Enter discount for your coupon."]
	},
	isActive : {
		type : Boolean,
		default : true
	}
})

module.exports = mongoose.model("Coupon", couponsSchema);

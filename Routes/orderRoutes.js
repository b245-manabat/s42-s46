const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const orderController = require("../Controllers/orderController");

router.put("/add/:id", orderController.addToCart);
router.put("/view", orderController.viewOrder);
router.get("/all", auth.verify, orderController.allOrder);
router.get("/purchased", orderController.isPurchased);
router.get("/delivered", orderController.isDelivered);
router.get("/received", orderController.isReceived);
router.get("/cancelled", orderController.isCancelled);

module.exports = router;
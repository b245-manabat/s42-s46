const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const profileController = require("../Controllers/profileController.js")

router.post("/page", profileController.viewProfile);
router.put("/page-update", profileController.editProfile);

module.exports = router;
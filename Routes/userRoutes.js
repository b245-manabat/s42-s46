const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const userController = require("../Controllers/userController.js")

router.post("/register", userController.userRegistration);
router.post("/login", userController.userAuthentication);
router.get("/details", userController.userDetails);
router.get("/nonadmin", auth.verify, userController.nonAdmin);
router.get("/admin", auth.verify, userController.admin);
router.put("/change-password", userController.changePassword);
router.put("/setadmin", auth.verify, userController.userAdmin);
router.put("/setnonadmin", auth.verify, userController.userNonAdmin);

module.exports = router;
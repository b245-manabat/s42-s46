const jwt = require("jsonwebtoken");
const shofee = "onlineShop";

module.exports.createAccessToken = (user) =>{
	
	const data = {
		_id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, shofee, {});
}

module.exports.verify = (request, response, next) =>{
	let token = request.headers.authorization;

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, shofee, (error, data)=>{
			if(error){
				return response.send({auth: "Failed."});
			}
			else{
				next();
			}
		})
	}
	else{
		return response.send({auth: "Failed."})
	}
}

module.exports.decode = (token) => {
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, shofee, (error, data)=>{
			if(error){
				return null;
			}
			else{
				return jwt.decode(token, {complete:true}).payload;
			}
		})
	}
	else{
		return null;
	}
}